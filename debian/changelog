intake (0.6.5-2) UNRELEASED; urgency=medium

  * Team Upload
  * add autopkgtest (Closes: #970312)

 -- Mohammed Bilal <mdbilal@disroot.org>  Sat, 02 Apr 2022 22:47:01 +0000

intake (0.6.5-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * New upstream version

  [ Étienne Mollier ]
  * Add mlist-parameter-test.patch: fix build time test failure.

 -- Andreas Tille <tille@debian.org>  Mon, 17 Jan 2022 07:46:16 +0100

intake (0.6.4-2) unstable; urgency=medium

  * adjust fix-ftbfs-on-32bits.patch to allow unit tests to pass on big endian
    systems, and rename the patch as architectures-support.patch, to reflect
    that change.

 -- Étienne Mollier <emollier@debian.org>  Sat, 16 Oct 2021 23:16:30 +0200

intake (0.6.4-1) unstable; urgency=medium

  * New upstream version
  * add sphinx-app-css.patch; replace obsolete add_stylesheet directive.
  * d/rules: copy more test artifacts as needed by the new version.

 -- Étienne Mollier <emollier@debian.org>  Sat, 16 Oct 2021 17:55:41 +0200

intake (0.6.3-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * Set upstream metadata fields: Contact.
  * refresh fix_privacy_breach.patch
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete field Contact from debian/upstream/metadata (already present
    in machine-readable debian/copyright).
  * fix_tests.patch: skip new tests needing internet.
  * intake-server.1: new manual page.
  * add typo-in-manual-page.patch to fix typos in intake(1) caught by lintian.
  * d/copyright: adjust copyright years per files content.
  * d/copyright: add myself as author of intake-server(1) manual page.

 -- Étienne Mollier <emollier@debian.org>  Wed, 22 Sep 2021 22:25:57 +0200

intake (0.6.1-2) unstable; urgency=medium

  * add fix-ftbfs-on-32bits.patch (Closes: #987486)
  * d/watch: fix broken link to github
  * d/control: add myself as uploader

 -- Étienne Mollier <emollier@debian.org>  Sun, 18 Jul 2021 22:19:14 +0200

intake (0.6.1-1) unstable; urgency=high

  * Team upload.
  * New upstream version, Closes: #982699
  * Standards-Version: 4.5.1 (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Refresh patches
  * Add ${sphinxdoc:Built-Using} for the -doc package

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 15 Mar 2021 11:26:46 +0100

intake (0.6.0-4) unstable; urgency=medium

  * Team upload

  [ Shayan Doust ]
  * Add some missing build dependencies
  * Fix some tests
  * Fix sphinx issue (Closes: #971177)

  [ Andreas Tille ]
  * Build-Depends: python3-tqdm
  * Ignore test that fails when more than one Python3 versions are
    installed

 -- Andreas Tille <tille@debian.org>  Mon, 16 Nov 2020 10:54:06 +0100

intake (0.6.0-3) unstable; urgency=medium

  * Team upload.
  * Install manpages only in python3-intake
    Closes: #965183

 -- Andreas Tille <tille@debian.org>  Fri, 17 Jul 2020 14:54:41 +0200

intake (0.6.0-2) unstable; urgency=medium

  * Team upload.
  * Respect DEB_BUILD_OPTIONS in override_dh_auto_test target (routine-
    update)
  * Remove trailing whitespace in debian/control (routine-update)
  * Remove trailing whitespace in debian/rules (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Tue, 14 Jul 2020 23:20:40 +0200

intake (0.6.0-1) unstable; urgency=medium

  * Initial release (Closes: #963160)

 -- Shayan Doust <hello@shayandoust.me>  Mon, 15 Jun 2020 18:20:20 +0100
